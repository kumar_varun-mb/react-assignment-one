import React from "react";

function DisplayComponent(props){
    return (
        <div className="displayComponent">
            {console.log(props)}
            <span>{(props.time.h > 9)? props.time.h :"0"+props.time.h}</span>&nbsp;:&nbsp;
            <span>{(props.time.m > 9)? props.time.m :"0"+props.time.m}</span>&nbsp;:&nbsp;
            <span>{(props.time.s > 9)? props.time.s :"0"+props.time.s}</span>&nbsp;:&nbsp;
            <span>{(props.time.ms > 9)? props.time.ms :"0"+props.time.ms}</span>&nbsp;
            {/* <span>{props.time.h}</span>&nbsp; */}
        </div>
    )
}

export default DisplayComponent;