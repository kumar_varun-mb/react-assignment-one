import React, { Component } from 'react'
import DisplayComponent from "./DisplayComponent";
import ButtonComponent from "./ButtonComponent";

class StopWatch extends Component {

    constructor() {
        super()
        this.state = {
            h: 0,
            m: 0,
            s: 0,
            ms: 0
        }
    }

    increment = () => {
        this.setState((prevState) => ({
            ms: prevState.ms + 1
        }))
        if (this.state.ms == 100) {
            this.setState((prevState) => ({
                s: prevState.s + 1,
                ms: 0
            }))
        }
        if (this.state.s == 60) {
            this.setState((prevState) => ({
                m: prevState.m + 1,
                s: 0
            }))
        }
        if (this.state.m == 60) {
            this.setState((prevState) => ({
                h: prevState.h + 1,
                m: 0
            }))
        }
    }

    
    start = () => {
        this.timer = setInterval(this.increment, 10);
    }
    pause = () => {
        clearInterval(this.timer);
    }
    reset = () => {
        clearInterval(this.run);
        this.setState((prevState) => ({
            h: 0,
            m: 0,
            s: 0,
            ms: 0
        }))
    }

    render() {
        return (
            <div>
                <DisplayComponent time={this.state} />
                <button onClick={() => this.start()}>Start</button>
                <button onClick={() => this.pause()}>Pause</button>
                <button onClick={() => this.reset()}>Reset</button>
                <ButtonComponent />
            </div>
        )
    }
}

export default StopWatch;
